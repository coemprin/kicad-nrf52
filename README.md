## Schématique d'une carte pour le KeyFinder

Ce projet vise à mapper une carte  électronique pour le keyFinder sur le logiciel Kicad. Le microcontroleur est la puce nrf52 et il devra être relié à :
- un bouton
- une led
- un buzzer
- un bouton reset
- un emplacement pour mettre une pile(CR2032)
- un connecteur programmation (JTAG)

Des composants supplémentaires seront rajoutés pour faire fonctionner les différents composants.
De plus, des références pour acheter les différents composant ont été ajoutés.
J'ai essayé de réduire la taille prise par l'ensemble des composants.

Importez le projet sur votre ordinateur puis ouvrez-le sur Kicad.

Les fichiers compilés sont le dossier output. 